## Prerequisites

* sudo apt update
* sudo apt install -y python3 python3-pip

* sudo pip3 install -U pip
* sudo pip3 install flask flask_cors sqlalchemy

#### In case of mysql database
* sudo apt-get install python3-dev libmysqlclient-dev
* sudo apt-get install python3-dev
* sudo pip install mysqlclient

##### Inside folder 'mysqlclient'
* sudo python3 setup.py install





## Installation

* git clone https://yan0s_ncsrd@bitbucket.org/mnlab-dev/monitoring-api.git
* cd /monitoring-api/

###### You can choose DataBase Type : MySQL or sqlite

###### Edit "main.conf" with the field "type". Write "mysql" or "sqlite".
```
[DATABASE]
type    = sqlite
name    = test
conn_string = ://root:char1234@localhost

```

###### Edit "main.conf" file with the "port" you wish the application to run at
```
[DEFAULT]
port    = 8082
```

###### In "main.conf" file edit PROMETHEUS file directories for testing purposes if they don't exist 
```
[Prometheus]
#target_resource_directory = /etc/prometheus/tgroups
target_resource_directory = ./
#alert_rule_directory      = /etc/prometheus/rules
alert_rule_directory      = ./
```

## Run

* python3 main.py


## Use

###### Using Postman application to test interfaces

* Go to "Import" > "Import File" > "Choose Files"
* Choose file monitoring-api/postman/char-monitoring.json

###### Current available features are:
* Create, Delete Database 
* Register, Get, Edit, Delete User
* Register, Get, Edit, Delete Target Resource
* Register, Get, Edit, Delete Alert Rule


