from flask import Blueprint, request, jsonify
from controllers.AlertNotificationController import AlertNotificationController
from helpers.CommonHelper import getValIfKeyExists
import json

alert_notification_api = Blueprint('alert_notification_api', __name__, template_folder='templates')


@alert_notification_api.route('/create_alert_notification/v1', methods=['POST'])
def v1_register_alert_notification():
    json_content = request.json
    print(json_content)
    alert_notification_controller = AlertNotificationController()
    response = alert_notification_controller.registerAlertNotification(json_content)
    return jsonify(response)


@alert_notification_api.route('/get_alert_notification/v1', methods=['POST'])
def v1_get_alert_notification():
    json_content = request.json
    alert_notification_controller = AlertNotificationController()
    response = alert_notification_controller.getAlertNotification(json_content)
    return jsonify(response)


@alert_notification_api.route('/update_alert_notification/v1', methods=['PUT'])
def v1_update_alert_notification():
    json_content = request.json
    alert_notification_controller = AlertNotificationController()
    response = alert_notification_controller.updateAlertNotificationEndTime(json_content)
    return jsonify(response)


@alert_notification_api.route('/delete_alert_notification/v1', methods=['DELETE'])
def v1_delete_alert_notification():
    alert_notification_controller = AlertNotificationController()
    json_content = {
                    "alert_notification": {
                        "id": int(request.args.get('id'))
                    }
                   }
    print(json_content)
    response = alert_notification_controller.deleteAlertNotification(json_content)
    return jsonify(response)
