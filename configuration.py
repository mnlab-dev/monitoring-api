import os
import configparser
import getpass


class config:

    # --------------------------  Global Variables  -----------------------------
    # Base Directory
    BASE_DIR = os.path.dirname(__file__)
    BASE_USER_DIR = os.path.join("/home", getpass.getuser())

    # Configuration File Directory
    CONF_FILE_DIR = os.path.join(BASE_DIR, "main.conf")

    config = configparser.ConfigParser()
    config.read(CONF_FILE_DIR)


    # -------------------------- Default --------------------------------------------

    # Server Port
    PORT = int(config['DEFAULT']['port'])
    ALLOW_CORS_FROM = config['DEFAULT']['allow_cors_from']

    # -------------------------- Database -------------------------------------------

    # Database Path
    DATABASE_TYPE = config ['DATABASE']['type']
    DATABASE_PATH = os.path.join(BASE_DIR, "db", config['DATABASE']['name'])
    DATABASE_CONN_STRING = config ['DATABASE']['conn_string']
    DATABASE_NAME = config ['DATABASE']['name']
    # -------------------------- Prometheus -------------------------------------------

    PROMETHEUS_TARGET_RESOURCE_DIR = config["PROMETHEUS"]["target_resource_directory"]
    PROMETHEUS_ALERT_RULE_DIR = config["PROMETHEUS"]["alert_rule_directory"]
    PROMETHEUS_NEW_VERSION = config["PROMETHEUS"]["new_version"]
