import os
import logging
from models import AlertNotification
from configuration import config as cnf
from helpers.DbHelper import db_session

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


class AlertNotificationController:

    def registerAlertNotification(self, json_content):
        alert_notification = AlertNotification.AlertNotification()

        with db_session() as db:
            alert_notification.set(json_content)
            new_id = alert_notification.create(db)
            serialized_obj = alert_notification.serialize_w_title
            db.commit()
        response = {"id": serialized_obj["alert_notification"]["id"]}
        return response

    def getAlertNotification(self, json_content):
        alert_notification = AlertNotification.AlertNotification()
        response = []
        if json_content is not None and bool(json_content) is True:
            alert_notification.set(json_content)
            with db_session() as db:
                response = alert_notification.get_by_filter(db)
        return response

    def updateAlertNotificationEndTime(self, dict_content):
        alert_notification = AlertNotification.AlertNotification()
        print(alert_notification)
        # if json_content is not None and bool(json_content) is True:
        alert_notification.set_f_edit(dict_content)
        response = "Nothing done.."
        with db_session() as db:
            response = alert_notification.update(db)
        # response = {"response": "update_done"}
        return response

    def deleteAlertNotification(self, json_content):
        alert_notification = AlertNotification.AlertNotification()
        alert_notification.id = json_content["alert_notification"]["id"]
        with db_session() as db:
            # Delete from db
            alert_notification.delete(db)
            db.commit()
        return {"response": "delete_done"}
