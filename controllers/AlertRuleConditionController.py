import json
import logging
from pprint import pprint

from models import AlertRuleCondition

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


class AlertRuleConditionController:

    def registerAlertRuleCondition(self, json_content):
        alert_rule_condition = AlertRuleCondition.AlertRuleCondition()
        alert_rule_condition.set(json_content)
        response = {"id": alert_rule_condition.create()}
        return response

    # def getAlertRule(self, json_content):
    #     alert_rule = AlertRule.AlertRule()
    #     if json_content is not None and bool(json_content) is True:
    #         alert_rule.set(json_content)
    #     response = alert_rule.get_by_filter()
    #     return response
    #
    # def editAlertRule(self, json_content):
    #     alert_rule = AlertRule.AlertRule()
    #     if json_content is not None and bool(json_content) is True:
    #         alert_rule.set(json_content)
    #     response = {"response": alert_rule.update()}
    #     return response
    #
    # def deleteAlertRule(self, json_content):
    #     alert_rule = AlertRule.AlertRule()
    #     alert_rule.id = json_content["alert_rule"]["id"]
    #     response = {"response": alert_rule.delete()}
    #     return response
