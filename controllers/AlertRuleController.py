import os
import logging
from models import AlertRule
from configuration import config as cnf
from helpers.DbHelper import db_session
from helpers.TemplateHelper import create_template
from helpers.CommonHelper import reload_service

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)


class AlertRuleController:

    def registerAlertRule(self, json_content):
        alert_rule = AlertRule.AlertRule()

        with db_session() as db:
            alert_rule.set(json_content)
            new_id = alert_rule.create(db)
            serialized_obj = alert_rule.serialize_w_title
            if cnf.PROMETHEUS_NEW_VERSION == "yes":
                create_template(serialized_obj, "alert_rule_v2.j2",
                                os.path.join(cnf.PROMETHEUS_ALERT_RULE_DIR, str(new_id) + ".yml"))
            else:
                create_template(serialized_obj, "alert_rule.j2",
                                os.path.join(cnf.PROMETHEUS_ALERT_RULE_DIR, str(new_id) + ".yml"))
            db.commit()
        reload_service("prometheus")
        reload_service("alertmanager")
        response = {"id": serialized_obj["alert_rule"]["id"]}
        return response

    def getAlertRule(self, json_content):
        alert_rule = AlertRule.AlertRule()
        response = []
        if json_content is not None and bool(json_content) is True:
            alert_rule.set(json_content)
            with db_session() as db:
                response = alert_rule.get_by_filter(db)
        return response

    def editAlertRule(self, json_content):
        alert_rule = AlertRule.AlertRule()
        # if json_content is not None and bool(json_content) is True:
        alert_rule.set_f_edit(json_content)
        with db_session() as db:
            alert_rule.delete_children(db)
            duration_id = alert_rule.create_duration(db, json_content)
            alert_rule.update(db, duration_id)
            alert_rule.create_condition(db, json_content)
            os.remove(os.path.join(cnf.PROMETHEUS_ALERT_RULE_DIR,
                                   str(json_content["alert_rule"]["id"]) + ".yml")
                      )
            create_template(json_content, "alert_rule.j2",
                            os.path.join(cnf.PROMETHEUS_ALERT_RULE_DIR, str(json_content["alert_rule"]["id"]) + ".yml"))
            db.commit()
        reload_service("prometheus")
        reload_service("alertmanager")
        response = {"response": "update_done"}
        return response

    def deleteAlertRule(self, json_content):
        alert_rule = AlertRule.AlertRule()
        alert_rule.id = json_content["alert_rule"]["id"]
        with db_session() as db:
            # Delete from db
            alert_rule.delete(db)
            # Delete file
            alert_rule_yml_filepath = os.path.join(cnf.PROMETHEUS_ALERT_RULE_DIR,
                                                   str(json_content["alert_rule"]["id"]) + ".yml")
            if os.path.isfile(alert_rule_yml_filepath):
                os.remove(alert_rule_yml_filepath)
            db.commit()
        reload_service("prometheus")
        reload_service("alertmanager")
        return {"response": "delete_done"}
