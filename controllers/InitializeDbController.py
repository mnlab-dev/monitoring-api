import logging

from sqlalchemy import create_engine, event

from configuration import config as cnf
from helpers.DbHelper import on_connect, db_session
from models import Base, Label, Duration, Offset, TargetResource, User, ExpressionMetric, ExpressionAlert, \
    ExpressionFunction, ExpressionFunctionLabelKey, Expression, AlertRule, AlertRuleCondition, \
    AlertRuleConditionInequality, AlertRuleConditionLogicalOperation, ExpressionMathematicalOperation, \
    Metric, MetricExporter, MetricExporterPort, AlertNotification, AlertNotificationMetadata
# from models.depreciated import Metric

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)



class InitializeDbController:

    def create_DB(self):

        mysqldbType = "mysql"
        connection_string = None
        # empty string
        connection_string = mysqldbType + cnf.DATABASE_CONN_STRING
        print(connection_string)
        # if connection_string.startswith('sqlite'):
        #     db_file = re.sub("sqlite.*:///", "", connection_string)
        #     os.makedirs(os.path.dirname(db_file))
        engine = create_engine(connection_string, echo=True)
        # event.listen(engine, 'connect', on_connect)
        conn = engine.connect()
        conn.execute("commit")
        conn.execute("CREATE DATABASE IF NOT EXISTS test;;")

        conn.close()

    def init_DB(self):


        mysqldbType = "mysql"
        sqlitedbType = "sqlite"
        sqlite_format = ":///"
        # Check if type of DB is sqlite or mysql
        connection_string = None
        # empty string
        engine = None

        if cnf.DATABASE_TYPE == mysqldbType:
            connection_string = mysqldbType + cnf.DATABASE_CONN_STRING + "/" + cnf.DATABASE_NAME
            engine = create_engine(connection_string)
        elif cnf.DATABASE_TYPE == sqlitedbType:
            connection_string = sqlitedbType + sqlite_format + cnf.DATABASE_PATH
            engine = create_engine(connection_string)
            event.listen(engine, 'connect', on_connect)
        print(connection_string)

        # if connection_string.startswith('sqlite'):
        #     db_file = re.sub("sqlite.*:///", "", connection_string)
        #     os.makedirs(os.path.dirname(db_file))

        # 3 commands for creating database

        base = Base.Base()
        TargetResource.TargetResource()
        Label.Label()
        Metric.Metric()
        User.User()
        Duration.Duration()
        Offset.Offset()

        Expression.Expression()
        ExpressionMetric.ExpressionMetric()
        ExpressionAlert.ExpressionAlert()
        ExpressionFunction.ExpressionFunction()
        ExpressionFunctionLabelKey.ExpressionFunctionLabelKey()
        ExpressionMathematicalOperation.ExpressionMathematicalOperation()

        AlertRule.AlertRule()
        AlertRuleCondition.AlertRuleCondition()
        AlertRuleConditionInequality.AlertRuleConditionInequality()
        AlertRuleConditionLogicalOperation.AlertRuleConditionLogicalOperation()

        Metric.Metric()
        MetricExporter.MetricExporter()
        MetricExporterPort.MetricExporterPort()

        AlertNotification.AlertNotification()
        AlertNotificationMetadata.AlertNotificationMetadata()

        base.metadata.create_all(engine)

        with db_session() as db:
            objects = [
                MetricExporter.MetricExporter(id=1, name="node_exporter"),
                MetricExporter.MetricExporter(id=2, name="ids_exporter"),
                MetricExporter.MetricExporter(id=3, name="cache_exporter"),
                MetricExporterPort.MetricExporterPort(port=9100, metric_exporter_id=1),
                MetricExporterPort.MetricExporterPort(port=10000, metric_exporter_id=2),
                MetricExporterPort.MetricExporterPort(port=9101, metric_exporter_id=3),
                Metric.Metric(name="cpu_used_percentage", metric_exporter_id=1),
                Metric.Metric(name="ram_used_percentage", metric_exporter_id=1),
                Metric.Metric(name="disk_used_percentage", metric_exporter_id=1),
                Metric.Metric(name="bytes_received", metric_exporter_id=1),
                Metric.Metric(name="bytes_transmitted", metric_exporter_id=1),
                Metric.Metric(name="packets_received", metric_exporter_id=1),
                Metric.Metric(name="packets_transmitted", metric_exporter_id=1),
                Metric.Metric(name="DDOS_Attack", metric_exporter_id=2),
                Metric.Metric(name="cache_total_hits", metric_exporter_id=3),
                Metric.Metric(name="cache_local_hit_ratio", metric_exporter_id=3),
                Metric.Metric(name="cache_sibling_hit_ratio", metric_exporter_id=3),
                Metric.Metric(name="cache_total_hit_ratio", metric_exporter_id=3),
                Metric.Metric(name="cache_local_hits_mean_time", metric_exporter_id=3),
                Metric.Metric(name="cache_peering_hits_mean_time", metric_exporter_id=3),
                Metric.Metric(name="cache_misses_mean_time", metric_exporter_id=3),
                Metric.Metric(name="cache_http_hits", metric_exporter_id=3),
                Metric.Metric(name="cache_sibling_hits", metric_exporter_id=3)
            ]
            db.bulk_save_objects(objects)
            db.flush()
            db.commit()

        response = "OK"
        return response


    def delete_DB(self):

        mysqldbType = "mysql"
        sqlitedbType = "sqlite"
        sqlite_format = "://"
        # Check if type of DB is sqlite or mysql
        connection_string = None
        # empty string
        engine  = None

        if cnf.DATABASE_TYPE == mysqldbType:
            connection_string = mysqldbType + cnf.DATABASE_CONN_STRING + "/" + cnf.DATABASE_NAME
            engine = create_engine(connection_string)
        elif cnf.DATABASE_TYPE == sqlitedbType:
            connection_string = sqlitedbType + sqlite_format + cnf.DATABASE_PATH
            engine = create_engine(connection_string)
            event.listen(engine, 'connect', on_connect)
        print(connection_string)

        base = Base.Base()
        for tbl in reversed(base.metadata.sorted_tables):
            tbl.drop(engine, checkfirst=True)

