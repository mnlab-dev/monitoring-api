import json
import logging
from pprint import pprint

from models import Label

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)



class LabelController:

    def registerLabel(self, json_content):
        label = Label.Label()
        label.set(json_content)

        response = {"id": label.create()}
        return response

    def getLabel(self, json_content):
        label = Label.Label()
        if json_content is not None and bool(json_content) is True:
            label.set(json_content)
        response = label.get_by_filter()
        return response

    def editLabel(self, json_content):
        label = Label.Label()
        if json_content is not None and bool(json_content) is True:
            label.set(json_content)
        response = {"response": label.update()}
        return response

    def deleteLabel(self, json_content):
        label = Label.Label()
        label.set(json_content)
        response = {"response": label.delete()}
        return response