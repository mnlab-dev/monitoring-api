import logging

from models import Metric

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)



class MetricController:

    def registerMetric(self, json_content):
        metric = Metric.Metric()
        metric.set(json_content)

        response = {"id": metric.create()}
        return response

    def getMetric(self, json_content):
        metric = Metric.Metric()
        if json_content is not None and bool(json_content) is True:
            metric.set(json_content)
        response = metric.get_by_filter()
        return response

    def editLabel(self, json_content):
        metric = Metric.Metric()
        if json_content is not None and bool(json_content) is True:
            metric.set(json_content)
        response = {"response": metric.update()}
        return response

    def deleteLabel(self, json_content):
        metric = Metric.Metric()
        metric.set(json_content)
        response = {"response": metric.delete()}
        return response