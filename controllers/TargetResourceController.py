import json
import logging
from pprint import pprint
import os
from models import TargetResource
from helpers.DbHelper import db_session
logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
from configuration import config as cnf
from helpers.TemplateHelper import create_template
from helpers.CommonHelper import reload_service


class TargetResourceController:

    def registerTargetResource(self, json_content):
        target_resource = TargetResource.TargetResource()
        with db_session() as db:
            target_resource.set(db, json_content)

        # os.environ['VAULT_PASS'] = 'just_a_pass'
        # os.environ['ANSIBLE_HOST_KEY_CHECKING'] = "False"
        # # local pc?
        # become_user_password = ''
        #
        # prometheus_node_exporter_metric_category_list = []
        # for exporter in target_resource.exporter_list:
        #     if exporter.type == "default":
        #         for metric_category in exporter.metric_category_list:
        #             prometheus_node_exporter_metric_category_list.append(metric_category.name)
        #
        #         run_data = {
        #             'user_id': 12345,
        #             "prometheus_node_exporter_metric_category_list": prometheus_node_exporter_metric_category_list,
        #             "host_key_checking": "False"
        #         }
        #
        #         runner = AnsibleHelper.Runner(
        #             inventory_list=[
        #                 {
        #                     "hostgroup_name": "host_group",
        #                     "server_list": [
        #                         {
        #                             "ip": target_resource.ip,
        #                             "ansible_user": target_resource.ssh_username,
        #                             "ansible_ssh_pass": target_resource.ssh_password,
        #                             "ansible_ssh_private_key_file": target_resource.ssh_private_key,
        #                         },
        #                     ],
        #                 },
        #             ],
        #             playbook='site.yml',
        #             run_data=run_data,
        #             become_pass=become_user_password,
        #             verbosity=0
        #         )
        #
        #         stats = runner.run()
        #
        #         print json.dumps(stats, default=lambda o: o.__dict__)
        #
        #         if stats.failures == {}:
        #             print "SUCCESS!!!"
        #         else:
        #             print "FAILURE..."
        #
        #     else:
        #         pass


            path_to_save = cnf.PROMETHEUS_TARGET_RESOURCE_DIR

            new_id = target_resource.create(db)
            serialized_obj = target_resource.serialize_w_title
            serialized_obj["target_resource"]["port_list"] = target_resource.get_ports(db)
            create_template(serialized_obj, "target_resource.j2",
                            os.path.join(path_to_save, str(new_id) + ".json"))
            db.commit()
        reload_service("prometheus")
        response = {"id": serialized_obj["target_resource"]["id"]}
        return response


    def getTargetResource(self, json_content):
        response = []
        target_resource = TargetResource.TargetResource()
        if json_content is not None and bool(json_content) is True:
            with db_session() as db:
                target_resource.set(db, json_content)
                response = target_resource.get_by_filter(db)
        return response

    def editTargetResource(self, json_content):
        prometheus_file_path = cnf.PROMETHEUS_TARGET_RESOURCE_DIR
        if json_content is not None and bool(json_content) is True:
            with db_session() as db:
                target_resource = db.query(TargetResource.TargetResource).get(json_content["target_resource"]["id"])
                target_resource.update(db, json_content)
                os.remove(os.path.join(prometheus_file_path,
                                       str(json_content["target_resource"]["id"]) + ".json")
                          )
                serialized_obj = target_resource.serialize_w_title
                serialized_obj["target_resource"]["port_list"] = target_resource.get_ports(db)
                create_template(serialized_obj, "target_resource.j2",
                                os.path.join(prometheus_file_path, str(json_content["target_resource"]["id"]) + ".json"))
                db.commit()
            response = {"response": "update_done"}
        else:
            response = {"response": "update_failed"}
        reload_service("prometheus")
        return response

    def deleteTargetResource(self, json_content):
        target_resource = TargetResource.TargetResource()
        target_resource.id = json_content["target_resource"]["id"]
        with db_session() as db:
            # Delete from DB
            target_resource.delete(db)
            # Delete target resource json file
            target_resource_json_filepath = os.path.join(cnf.PROMETHEUS_TARGET_RESOURCE_DIR,
                                   str(json_content["target_resource"]["id"]) + ".json")
            if os.path.isfile(target_resource_json_filepath):
                os.remove(target_resource_json_filepath)
            db.commit()
        reload_service("prometheus")
        return {"response": "delete_done"}

    def getMetricsAvailable(self):
        response = ""
        with db_session() as db:
            pass
        return response
