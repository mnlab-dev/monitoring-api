import json
import logging
from pprint import pprint

from models import User

logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)



class UserController:

    def registerUser(self, json_content):
        user = User.User()
        user.set(json_content)

        response = {"id": user.create()}
        return response

    def getUser(self, json_content):
        user = User.User()
        if json_content is not None and bool(json_content) is True:
            user.set(json_content)
        response = user.get_by_filter()
        return response

    def editUser(self, json_content):
        user = User.User()
        if json_content is not None and bool(json_content) is True:
            user.set(json_content)
        response = {"response": user.update()}
        return response

    def deleteUser(self, json_content):
        user = User.User()
        user.set(json_content)
        response = {"response": user.delete()}
        return response