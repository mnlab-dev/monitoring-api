import jinja2, os
from configuration import config as cnf

def create_template(serialized_obj, filename_template, filepath_to_save):

    # In this case, we will load templates off the filesystem.
    # This means we must construct a FileSystemLoader object.
    #
    # The search path can be used to make finding templates by
    #   relative paths much easier.  In this case, we are using
    #   absolute paths and thus set it to the filesystem root.
    templateLoader = jinja2.FileSystemLoader(searchpath=os.path.join(cnf.BASE_DIR, "helpers/templates"))

    # An environment provides the data necessary to read and
    #   parse our templates.  We pass in the loader object here.
    templateEnv = jinja2.Environment(loader=templateLoader, extensions=['jinja2.ext.loopcontrols'])

    # Read the template file using the environment object.
    # This also constructs our Template object.
    template = templateEnv.get_template(filename_template)

    # Finally, process the template to produce our final text.
    outputText = template.render(serialized_obj)

    print(outputText)

    file_to_create = open(filepath_to_save, "w+")
    file_to_create.write(outputText)
    file_to_create.close()
