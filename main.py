#!/usr/bin/python
#  -*- coding: utf8 -*-
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

from configuration import config as cnf
from controllers.AlertRuleConditionController import AlertRuleConditionController
from controllers.AlertRuleController import AlertRuleController
from controllers.InitializeDbController import InitializeDbController
from controllers.LabelController import LabelController
from controllers.MetricController import MetricController
from controllers.TargetResourceController import TargetResourceController
from controllers.UserController import UserController
from helpers.DbHelper import db_session
from models import TargetResource, Metric, MetricExporter, MetricExporterPort

from api.v1.alert_notification_api import alert_notification_api

app = Flask(__name__)
app.register_blueprint(alert_notification_api)
CORS(app)

# Additional headers to allow CORS
@app.after_request
def add_headers(response):
#    response.headers.add('Access-Control-Allow-Origin', cnf.ALLOW_CORS_FROM)
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization,X-Auth-Token,\
                                         X-Requested-With,withCredentials,Set-Cookie,accept,origin')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response


##########################################################
##################       DB       ########################
##########################################################


@app.route('/createDB', methods=['GET'])
def create_DB():
    mysqldbType = "mysql"
    sqlitedbType = "sqlite"
    initDB = InitializeDbController()

    #check if type of databse is mysql or sqlite
    if cnf.DATABASE_TYPE == mysqldbType:
        initDB.create_DB()
        initDB.init_DB()
    elif cnf.DATABASE_TYPE == sqlitedbType:
        initDB.init_DB()
    return "200"


@app.route('/deleteDB', methods=['DELETE'])
def delete_DB():
    initDB = InitializeDbController()
    initDB.delete_DB()
    return "200"

##################     User       ########################
##########################################################

@app.route('/register_user', methods=['POST'])
def register_user():
    json_content = request.json
    print(json_content)
    user_controller = UserController()
    response = user_controller.registerUser(json_content)
    return jsonify(response)


@app.route('/get_user', methods=['POST'])
def get_user():
    json_content = request.json
    print(json_content)
    user_controller = UserController()
    response = user_controller.getUser(json_content)
    return jsonify(response)


@app.route('/edit_user', methods=['PUT'])
def edit_user():
    json_content = request.json
    print(json_content)
    user_controller = UserController()
    response = user_controller.editUser(json_content)
    return jsonify(response)


@app.route('/delete_user', methods=['DELETE'])
def delete_user():
    user_controller = UserController()
    json_content = {
                    "user": {
                        "id": int(request.args.get('id'))
                    }
                   }
    print(json_content)
    response = user_controller.deleteUser(json_content)
    return jsonify(response)


##################   Label        ########################
##########################################################

@app.route('/register_label', methods=['POST'])
def register_label():
    json_content = request.json
    print(json_content)
    label_controller = LabelController()
    response = label_controller.registerLabel(json_content)
    return jsonify(response)


@app.route('/get_label', methods=['POST'])
def get_label():
    json_content = request.json
    print(json_content)
    label_controller = LabelController()
    response = label_controller.getLabel(json_content)
    return jsonify(response)


@app.route('/edit_label', methods=['PUT'])
def edit_label():
    json_content = request.json
    print(json_content)
    label_controller = LabelController()
    response = label_controller.editLabel(json_content)
    return jsonify(response)


@app.route('/delete_label', methods=['DELETE'])
def delete_label():
    label_controller = LabelController()
    json_content = {
                    "label": {
                        "id": int(request.args.get('id'))
                    }
                   }
    print(json_content)
    response = label_controller.deleteLabel(json_content)
    return jsonify(response)


##################   Metric       ########################
##########################################################

@app.route('/register_metric', methods=['POST'])
def register_metric():
    json_content = request.json
    print(json_content)
    metric_controller = MetricController()
    response = metric_controller.registerMetric(json_content)
    return jsonify(response)


@app.route('/get_metric', methods=['POST'])
def get_metric():
    json_content = request.json
    print(json_content)
    metric_controller = MetricController()
    response = metric_controller.getMetric(json_content)
    return jsonify(response)


@app.route('/edit_metric', methods=['PUT'])
def edit_metric():
    json_content = request.json
    print(json_content)
    metric_controller = MetricController()
    response = metric_controller.editMetric(json_content)
    return jsonify(response)


@app.route('/delete_metric', methods=['DELETE'])
def delete_metric():
    metric_controller = MetricController()
    json_content = {
                    "metric": {
                        "id": int(request.args.get('id'))
                    }
                   }
    print(json_content)
    response = metric_controller.deleteMetric(json_content)
    return jsonify(response)


##################     Target Resource   #################
##########################################################


@app.route('/register_target_resource', methods=['POST'])
def register_target_resource():
    json_content = request.json
    print(json_content)
    target_resource_controller = TargetResourceController()
    response = target_resource_controller.registerTargetResource(json_content)
    return jsonify(response)


@app.route('/get_target_resource', methods=['POST'])
def get_target_resource():
    json_content = request.json
    target_resource_controller = TargetResourceController()
    response = target_resource_controller.getTargetResource(json_content)
    return jsonify(response)


@app.route('/edit_target_resource', methods=['PUT'])
def edit_target_resource():
    json_content = request.json
    target_resource_controller = TargetResourceController()
    response = target_resource_controller.editTargetResource(json_content)
    return jsonify(response)


@app.route('/delete_target_resource', methods=['DELETE'])
def delete_target_resource():
    target_resource_controller = TargetResourceController()
    json_content = {
                    "target_resource": {
                        "id": int(request.args.get('id'))
                    }
                   }
    print(json_content)
    response = target_resource_controller.deleteTargetResource(json_content)
    return jsonify(response)


@app.route('/get_metrics_available', methods=['GET'])
def get_metrics_available():
    # target_resource_controller = TargetResourceController()
    # response = target_resource_controller.delet()
    response = []

    with db_session() as db:
        metrics = db.query(Metric.Metric).all()
        metrics_serialized = []
        for metric in metrics:
            metric_serialized = metric.serialize_w_title
            metrics_serialized.append(metric_serialized)
    response = metrics_serialized
    return jsonify(response)

##################     Alert Rule   #################
##########################################################


@app.route('/register_alert_rule', methods=['POST'])
def register_alert_rule():
    json_content = request.json
    print(json_content)
    alert_rule_controller = AlertRuleController()
    response = alert_rule_controller.registerAlertRule(json_content)
    return jsonify(response)


@app.route('/get_alert_rule', methods=['POST'])
def get_alert_rule():
    json_content = request.json
    alert_rule_controller = AlertRuleController()
    response = alert_rule_controller.getAlertRule(json_content)
    return jsonify(response)


@app.route('/edit_alert_rule', methods=['PUT'])
def edit_alert_rule():
    json_content = request.json
    alert_rule_controller = AlertRuleController()
    response = alert_rule_controller.editAlertRule(json_content)
    return jsonify(response)


@app.route('/delete_alert_rule', methods=['DELETE'])
def delete_alert_rule():
    alert_rule_controller = AlertRuleController()
    json_content = {
                    "alert_rule": {
                        "id": int(request.args.get('id'))
                    }
                   }
    print(json_content)
    response = alert_rule_controller.deleteAlertRule(json_content)
    return jsonify(response)


##################    Alert Rule Condition   #################
##########################################################


@app.route('/register_alert_rule_condition', methods=['POST'])
def register_alert_rule_condition():
    json_content = request.json
    print(json_content)
    alert_rule_condition_controller = AlertRuleConditionController()
    response = alert_rule_condition_controller.registerAlertRuleCondition(json_content)
    return jsonify(response)



@app.route('/test', methods=['GET', 'POST'])
def test():
    def get_distinct(seq):
        seen = set()
        seen_add = seen.add
        return [x for x in seq if not (x in seen or seen_add(x))]

    response = []
    with db_session() as db:
        res = db.query(MetricExporterPort.MetricExporterPort.port)\
            .join(Metric.Metric, TargetResource.TargetResource.metric_list)\
            .join(MetricExporter.MetricExporter, Metric.Metric.metric_exporter)\
            .join(MetricExporterPort.MetricExporterPort, MetricExporter.MetricExporter.metric_exporter_port_list)\
            .order_by(MetricExporterPort.MetricExporterPort.port).all()
        print(res)
        print(get_distinct(res))
    #     items_serialized = []
    #     for item in res:
    #         item_serialized = item.serialize_w_title
    #         items_serialized.append(item_serialized)
    # response = items_serialized
    # return jsonify(response)
    return "All good!"


##########################################################

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=cnf.PORT, threaded=True)


