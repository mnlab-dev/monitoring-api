from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, and_, DateTime
from sqlalchemy.orm import relationship, backref
from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists
from datetime import datetime

from sqlalchemy import desc

class AlertNotification(Base):
    __tablename__ = 'alert_notification'

    id = Column(Integer, primary_key=True)
    name = Column(String(64), nullable=True)
    description = Column(String(256), nullable=True)
    start_time = Column(DateTime, nullable=True)
    end_time = Column(DateTime, nullable=True)

    user_id = Column(Integer, ForeignKey('user.id', onupdate="cascade", ondelete="cascade"), nullable=False)

    # Intentionally left nullable as notifications could be of interest even after the alert rule is deleted
    alert_rule_id = Column(Integer, ForeignKey('alert_rule.id', onupdate="SET NULL", ondelete="SET NULL"), nullable=True)

    alert_notification_metadata_list = relationship("AlertNotificationMetadata", cascade="all,delete",
                                                    back_populates="alert_notification")

    def __repr__(self):
        return "<AlertNotification(id='%s', name='%s', description='%s', start_time='%s', " \
               "end_time='%s', user_id='%s', alert_rule_id='%s')>" % \
               (self.id, self.name, self.description, self.start_time,
                self.end_time, self.user_id, self.alert_rule_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "start_time": self.start_time,
            "end_time": self.end_time,
            "alert_rule_id": self.alert_rule_id,
            "user_id": self.user_id,
            "alert_metadata_list": self.serialize_many2many(self.alert_notification_metadata_list)
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "alert_notification": self.serialize
        }
        return serialized_obj

    def serialize_many2many(self, many2many_property):
        return [item.serialize for item in many2many_property]

    def set(self, json_representation):
        from .AlertNotificationMetadata import AlertNotificationMetadata
        # json_representation = json_representation["alert_notification"]
        start_time = getValIfKeyExists(json_representation, "start_time")
        end_time = getValIfKeyExists(json_representation, "end_time")

        self.id = getValIfKeyExists(json_representation, "id")
        self.name = getValIfKeyExists(json_representation, "name")
        self.description = getValIfKeyExists(json_representation, "description")
        if start_time is not None:
            self.start_time = datetime.fromtimestamp(start_time)
        if end_time is not None:
            self.end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')
        self.alert_rule_id = getValIfKeyExists(json_representation, "alert_rule_id")
        self.user_id = getValIfKeyExists(json_representation, "user_id")

        for alert_notification_metadata in getValIfKeyExists(json_representation, "alert_metadata_list"):
            alert_notification_metadata_temp = AlertNotificationMetadata()
            alert_notification_metadata_temp.set(alert_notification_metadata)
            self.alert_notification_metadata_list.append(alert_notification_metadata_temp)

    def set_f_edit(self, dict_representation):
        # dict_representation = dict_representation["alert_rule"]

        # TODO: getValIfExists should be a class method. In case there is no change it should do nothing.
        # TODO:                Right now it will change the value to None!
        self.alert_rule_id = getValIfKeyExists(dict_representation, "alert_rule_id")
        end_time = getValIfKeyExists(dict_representation, "end_time")
        if end_time is not None:
            self.end_time = datetime.fromtimestamp(end_time)


    def create(self, db):
        db.add(self)
        db.flush()
        db.refresh(self)
        return_id = self.id
        return return_id

    def get_by_filter(self, db):
        alert_rules = db.query(AlertNotification) \
                       .filter(AlertNotification.id == self.id if self.id is not None else True)\
                       .filter(AlertNotification.name == self.name if self.name is not None else True)\
                       .filter(AlertNotification.description == self.name if self.name is not None else True)\
                       .filter(AlertNotification.user_id == self.user_id if self.user_id is not None else True)\
                       .all()

        alert_rules_json = []
        for alert_rule in alert_rules:
            alert_rule_json = alert_rule.serialize_w_title
            alert_rules_json.append(alert_rule_json)
        return alert_rules_json

    def get_latest_start_time(self, db):
        alert_rules = db.query(AlertNotification) \
                       .filter(AlertNotification.id == self.id if self.id is not None else True)\
                       .filter(AlertNotification.name == self.name if self.name is not None else True)\
                       .filter(AlertNotification.description == self.name if self.name is not None else True)\
                       .filter(AlertNotification.user_id == self.user_id if self.user_id is not None else True)\
                       .order_by(desc(AlertNotification.start_time))\
                       .all()

        alert_rules_json = []
        for alert_rule in alert_rules:
            alert_rule_json = alert_rule.serialize_w_title
            alert_rules_json.append(alert_rule_json)
        return alert_rules_json

    def update(self, db):
        print(self)
        # temp_var = db.query(AlertNotification) \
        #            .filter(AlertNotification.alert_rule_id == 1)\
        #            .order_by(desc(AlertNotification.start_time))\
        #            .limit(1)\
        #            .one()

        temp_alert_notifications = db.query(AlertNotification) \
                   .filter(AlertNotification.alert_rule_id == self.alert_rule_id)\
                   .filter(AlertNotification.end_time == None)\
                   .all()

        for alert_notification in temp_alert_notifications:
            alert_notification.end_time = self.end_time

        db.flush()
        db.commit()
        return "update_done"

    def delete(self, db):
        temp_obj = db.query(AlertNotification).get(self.id)
        db.delete(temp_obj)
        db.flush()
