from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, and_, Float
from sqlalchemy.orm import relationship, backref
from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists


class AlertNotificationMetadata(Base):
    __tablename__ = 'alert_notification_metadata'

    id = Column(Integer, primary_key=True)
    metric_name = Column(String(64), nullable=False)
    metric_value = Column(Float, nullable=False)

    # onupdate="cascade", ondelete="cascade" does not implements cascading -  cascade="all,delete" on the parent does!
    alert_notification_id = Column(Integer, ForeignKey('alert_notification.id'), nullable=False)

    alert_notification = relationship("AlertNotification", back_populates="alert_notification_metadata_list")

    label_assoc_table = Table('alert_notification_meta__assoc__label', Base.metadata,
                               Column('alert_notification_metadata_id', Integer,
                                      ForeignKey('alert_notification_metadata.id', onupdate="cascade",
                                                 ondelete="cascade"),
                                      primary_key=True),
                               Column('label_id', Integer,
                                      ForeignKey('label.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True)
                               )

    label_list = relationship("Label", secondary='alert_notification_meta__assoc__label',
                              backref="alert_notification_metadata", cascade="all,delete")

    def __repr__(self):
        return "<AlertNotificationMetadata(id='%s', metric_name='%s', metric_value='%s', " \
               "alert_notification_id='%s')>" % \
               (self.id, self.metric_name, self.metric_value, self.alert_notification_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "metric_name": self.metric_name,
            "metric_value": self.metric_value,
            "label_list": self.serialize_many2many(self.label_list),
            "alert_notification_id": self.alert_notification_id
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "alert_notification_metadata": self.serialize
        }
        return serialized_obj

    def serialize_many2many(self, many2many_property):
        return [item.serialize_w_title for item in many2many_property]


    def set(self, json_representation):
        from .Label import Label
        # json_representation = json_representation["alert_notification_metadata"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.metric_name = getValIfKeyExists(json_representation, "metric_name")
        self.metric_value = getValIfKeyExists(json_representation, "metric_value")
        self.alert_notification_id = getValIfKeyExists(json_representation, "alert_notification_id")

        for label in getValIfKeyExists(json_representation, "parameter_list"):
            label_temp = Label()
            label_temp.set(label)
            self.label_list.append(label_temp)
