from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, and_
from sqlalchemy.orm import relationship, backref
from models.Base import Base
from helpers.DbHelper import on_connect, delete_label_orphans, db_session
from helpers.CommonHelper import getValIfKeyExists

class AlertRule(Base):
    __tablename__ = 'alert_rule'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=True)
    annotation_summary = Column(String(256), nullable=True)
    annotation_description = Column(String(512), nullable=True)
    # This foreign key should never be Null but is allowed only for the transitional stage during the update operation
    duration_id = Column(Integer, ForeignKey('duration.id', onupdate="cascade", ondelete="SET NULL"), nullable=True)

    user_id = Column(Integer, ForeignKey('user.id', onupdate="cascade", ondelete="cascade"), nullable=False)

    duration = relationship("Duration", foreign_keys='AlertRule.duration_id')

    condition = relationship("AlertRuleCondition", back_populates="alert_rule", uselist=False)

    # TODO: Add "Label" relationship

    def __repr__(self):
        return "<AlertRule(id='%s', name='%s', annotation_summary='%s', annotation_description='%s', " \
               "duration_id='%s', condition_id='%s')>" % \
               (self.id, self.name, self.annotation_summary, self.annotation_description,
                self.duration_id, self.condition_id)

    @property
    def serialize(self):
        serialized_obj = {
                "id": self.id,
                "name": self.name,
                "annotation_summary": self.annotation_summary,
                "annotation_description": self.annotation_description,
                "duration": self.duration.serialize if self.duration is not None else None,
                "condition": self.condition.serialize if self.condition is not None else None,
                "user_id": self.user_id
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "alert_rule": self.serialize
        }
        return serialized_obj

    def set(self, json_representation):
        from .Duration import Duration
        from .AlertRuleCondition import AlertRuleCondition

        json_representation = json_representation["alert_rule"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.name = getValIfKeyExists(json_representation, "name")
        self.annotation_summary = getValIfKeyExists(json_representation, "annotation_summary")
        self.annotation_description = getValIfKeyExists(json_representation, "annotation_description")
        self.user_id = getValIfKeyExists(json_representation, "user_id")
        if getValIfKeyExists(json_representation, "duration") is not None:
            self.duration = Duration()
            self.duration.set(json_representation)
        if getValIfKeyExists(json_representation, "condition") is not None:
            self.condition = AlertRuleCondition()
            self.condition.set(json_representation)

    def set_f_edit(self, json_representation):
        json_representation = json_representation["alert_rule"]

        # TODO: getValIfExists should be a class method. In case there is no change it should do nothing.
        # TODO:                Right now it will change the value to None!
        self.id = getValIfKeyExists(json_representation, "id")
        self.name = getValIfKeyExists(json_representation, "name")
        self.annotation_summary = getValIfKeyExists(json_representation, "annotation_summary")
        self.annotation_description = getValIfKeyExists(json_representation, "annotation_description")
        self.user_id = getValIfKeyExists(json_representation, "user_id")
        self.duration = None
        self.condition = None

    def create(self, db):
        db.add(self)
        db.flush()
        db.refresh(self)
        return_id = self.id
        return return_id

    def get_by_filter(self, db):
        alert_rules = db.query(AlertRule) \
                       .filter(AlertRule.id == self.id if self.id is not None else True)\
                       .filter(AlertRule.name == self.name if self.name is not None else True)\
                       .filter(AlertRule.annotation_summary == self.annotation_summary
                                                              if self.annotation_summary is not None else True)\
                       .filter(AlertRule.annotation_description == self.annotation_description
                                                              if self.annotation_description is not None else True)\
                       .filter(AlertRule.user_id == self.user_id if self.user_id is not None else True)\
                       .all()

        alert_rules_json = []
        for alert_rule in alert_rules:
            alert_rule_json = alert_rule.serialize_w_title
            alert_rules_json.append(alert_rule_json)
        return alert_rules_json

    # TO BE further investigated
    def update(self, db, duration_id):
        self.duration_id = duration_id
        db.merge(self)
        db.flush()
        db.commit()
        return "update_done"

    def create_condition(self, db, serialized_obj):
        from .AlertRuleCondition import AlertRuleCondition

        temp_condition = AlertRuleCondition()
        temp_condition.set(serialized_obj["alert_rule"])
        temp_condition.alert_rule_id = serialized_obj["alert_rule"]["id"]
        db.add(temp_condition)
        db.flush()
        # db.commit()
        return "ok"

    def create_duration(self, db, serialized_obj):
        from .Duration import Duration

        temp_duration = Duration()
        temp_duration.set(serialized_obj["alert_rule"])
        db.add(temp_duration)
        db.flush()

        db.refresh(temp_duration)
        duration_id = temp_duration.id
        print(duration_id)

        db.flush()
        db.commit()
        return duration_id

    def delete_children(self, db):
        temp_alert_rule = db.query(AlertRule).get(self.id)
        if temp_alert_rule.condition is not None:
            db.delete(temp_alert_rule.condition)
            db.flush()
        if temp_alert_rule.duration is not None:
            db.delete(temp_alert_rule.duration)
            db.flush()
        return "delete_children_done"

    def delete(self, db):
        temp_obj = db.query(AlertRule).get(self.id)
        self.delete_children(db)
        db.delete(temp_obj)
        db.flush()


