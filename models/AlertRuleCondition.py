from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, Enum
from sqlalchemy.orm import relationship, backref
from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists
from helpers.DbHelper import db_session

class AlertRuleCondition(Base):
    __tablename__ = 'alert_rule_condition'

    id = Column(Integer, primary_key=True)
    category = Column(Enum('promql', 'inequality', 'logical_operation'), nullable=False)
    promql = Column(String(128), nullable=True)

    # This foreign key should never be Null but is allowed only for the transitional stage during the update operation
    alert_rule_id = Column(Integer, ForeignKey('alert_rule.id',
                                               onupdate="cascade", ondelete="cascade"),
                           nullable=True)

    inequality_id = Column(Integer, ForeignKey('alert_rule_condition_inequality.id',
                                               onupdate="cascade", ondelete="cascade"),
                           nullable=True)

    logical_operation_id = Column(Integer, ForeignKey('alert_rule_condition_logical_operation.id', onupdate="cascade",
                                                      ondelete="cascade"),
                                  nullable=True)

    inequality = relationship("AlertRuleConditionInequality", foreign_keys='AlertRuleCondition.inequality_id',
                              cascade="all,delete")

    alert_rule = relationship("AlertRule", foreign_keys='AlertRuleCondition.alert_rule_id',
                              back_populates="condition", uselist=False)
    ## Cascade here adds reverse cascade: when child is deleted parent is deleted too
    # alert_rule = relationship("AlertRule", foreign_keys='AlertRuleCondition.alert_rule_id', cascade="all,delete",
    #                           back_populates="condition", uselist=False)

    logical_operation = relationship("AlertRuleConditionLogicalOperation",
                                     foreign_keys='AlertRuleCondition.logical_operation_id', cascade="all,delete")

    def __repr__(self):
        return "<AlertRuleCondition(id='%s', category='%s', promql='%s', inequality_id='%s', " \
               "logical_operation_id='%s')>" % \
               (self.id, self.category, self.promql, self.inequality_id, self.logical_operation_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "category": self.category,
            "promql": self.promql,
            "inequality": self.inequality.serialize if self.inequality is not None else None,
            "logical_operation": self.logical_operation.serialize if self.logical_operation is not None else None
            # "inequality_id": self.inequality_id,
            # "logical_operation_id": self.logical_operation_id
        }

        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "condition": self.serialize
        }
        return serialized_obj

    def set(self, alert_rule_condition_json):
        from .AlertRuleConditionInequality import AlertRuleConditionInequality
        from .AlertRuleConditionLogicalOperation import AlertRuleConditionLogicalOperation

        alert_rule_condition_json = alert_rule_condition_json["condition"]

        self.id = getValIfKeyExists(alert_rule_condition_json, "id")
        self.category = getValIfKeyExists(alert_rule_condition_json, "category")
        self.promql = getValIfKeyExists(alert_rule_condition_json, "promql")
        self.inequality_id = getValIfKeyExists(alert_rule_condition_json, "inequality_id")
        self.logical_operation_id = getValIfKeyExists(alert_rule_condition_json, "logical_operation_id")
        self.alert_rule_id = getValIfKeyExists(alert_rule_condition_json, "alert_rule_id")


        if getValIfKeyExists(alert_rule_condition_json, "inequality") is not None:
            self.inequality = AlertRuleConditionInequality()
            self.inequality.set(alert_rule_condition_json)
        if getValIfKeyExists(alert_rule_condition_json, "logical_operation") is not None:
            self.logical_operation = AlertRuleConditionLogicalOperation()
            self.logical_operation.set(alert_rule_condition_json)


    def create(self):
        with db_session() as db:
            db.add(self)
            db.flush()

            db.refresh(self)
            return_id = self.id

            db.commit()
        return return_id
