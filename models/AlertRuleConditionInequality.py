from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, Enum
from sqlalchemy.orm import relationship
from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class AlertRuleConditionInequality(Base):
    __tablename__ = 'alert_rule_condition_inequality'

    id = Column(Integer, primary_key=True)
    comparison_operator = Column(Enum('==', '!=', '>', '<', '>=', '<='), nullable=False)

    lhs_expression_id = Column(Integer, ForeignKey('expression.id',
                                                   onupdate="cascade", ondelete="cascade"),
                               nullable=False)

    rhs_expression_id = Column(Integer, ForeignKey('expression.id',
                                                   onupdate="cascade", ondelete="cascade"),
                               nullable=False)

    lhs_expression = relationship("Expression", foreign_keys="AlertRuleConditionInequality.lhs_expression_id",
                                  cascade="all,delete")
    rhs_expression = relationship("Expression", foreign_keys="AlertRuleConditionInequality.rhs_expression_id",
                                  cascade="all,delete")

    def __repr__(self):
        return "<AlertRuleConditionInequality(id='%s', comparison_operator='%s', lhs_expression_id='%s', " \
               "rhs_expression_id='%s')>" % \
               (self.id, self.comparison_operator, self.lhs_expression_id, self.rhs_expression_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "comparison_operator": self.comparison_operator,
            "LHS": self.lhs_expression.serialize_w_title if self.lhs_expression is not None else None,
            "RHS": self.rhs_expression.serialize_w_title if self.rhs_expression is not None else None
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "inequality": self.serialize
        }
        return serialized_obj

    def set(self, json_representation):
        from .Expression import Expression

        json_representation = json_representation["inequality"]
        if json_representation is not None:
            self.id = getValIfKeyExists(json_representation, "id")
            self.comparison_operator = getValIfKeyExists(json_representation, "comparison_operator")
            if getValIfKeyExists(json_representation["LHS"], "expression") is not None:
                self.lhs_expression = Expression()
                self.lhs_expression.set(json_representation["LHS"])
            if getValIfKeyExists(json_representation["RHS"], "expression") is not None:
                self.rhs_expression = Expression()
                self.rhs_expression.set(json_representation["RHS"])
