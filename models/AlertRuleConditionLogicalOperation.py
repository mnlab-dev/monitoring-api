from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, Enum
from sqlalchemy.orm import relationship
from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class AlertRuleConditionLogicalOperation(Base):
    __tablename__ = 'alert_rule_condition_logical_operation'

    id = Column(Integer, primary_key=True)
    logical_operator = Column(Enum('AND', 'OR', 'UNLESS',), nullable=False)

    logical_operation__assoc__condition = Table('alert_rule_con_logical_operation__assoc__alert_rule_con',
                               Base.metadata,
                               Column('alert_rule_condition_logical_operation_id', Integer,
                                      ForeignKey('alert_rule_condition_logical_operation.id',
                                                 onupdate="cascade", ondelete="cascade"),
                                      primary_key=True),
                               Column('alert_rule_condition_id', Integer,
                                      ForeignKey('alert_rule_condition.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True)
                               )

    condition_list = relationship("AlertRuleCondition",
                                  secondary='alert_rule_con_logical_operation__assoc__alert_rule_con',
                                  backref="alert_rule_condition", cascade="all,delete")


    def __repr__(self):
        return "<AlertRuleConditionLogicalOperation(id='%s', logical_operator='%s', lhs_expression_id='%s', " \
               "rhs_expression_id='%s')>" % \
               (self.id, self.logical_operator, self.lhs_expression_id, self.rhs_expression_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "logical_operator": self.logical_operator,
            "condition_list": self.serialize_many2many(self.condition_list)
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "logical_operation": self.serialize
        }
        return serialized_obj

    def serialize_many2many(self, many2many_property):
        return [item.serialize_w_title for item in many2many_property]

    def set(self, json_representation):
        from .AlertRuleCondition import AlertRuleCondition

        json_representation = json_representation["logical_operation"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.logical_operator = getValIfKeyExists(json_representation, "logical_operator")

        for condition in getValIfKeyExists(json_representation, "condition_list"):
            condition_temp = AlertRuleCondition()
            condition_temp.set(condition)
            self.condition_list.append(condition_temp)
