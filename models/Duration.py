from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String, Enum

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class Duration(Base):
    __tablename__ = 'duration'

    id = Column(Integer, primary_key=True)
    value = Column(Integer, nullable=False)
    unit = Column(Enum('s', 'm', 'h', 'd', 'w', 'y'), nullable=False)

    def __repr__(self):
        return "<Duration(id='%s', value='%s', unit='%s')>" % \
               (self.id, self.value, self.unit)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "value": self.value,
            "unit": self.unit
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "duration": self.serialize
        }
        return serialized_obj

    def set(self, duration_json):
        duration_json = duration_json["duration"]

        self.id = getValIfKeyExists(duration_json, "id")
        self.value = getValIfKeyExists(duration_json, "value")
        self.unit = getValIfKeyExists(duration_json, "unit")

