from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String, Enum

from models.Base import Base
from sqlalchemy.orm import relationship
from helpers.CommonHelper import getValIfKeyExists

class Expression(Base):
    __tablename__ = 'expression'

    id = Column(Integer, primary_key=True)
    category = Column(Enum('promql', 'constant', 'metric', 'alert', 'mathematical_operation'), nullable=False)
    promql = Column(String(128), nullable=True)
    constant = Column(Integer, nullable=True)

    function_id = Column(Integer, ForeignKey('expression_function.id', onupdate="cascade", ondelete="cascade"),
                         nullable=True)

    metric_id = Column(Integer, ForeignKey('expression_metric.id', onupdate="cascade", ondelete="cascade"),
                       nullable=True)

    alert_id = Column(Integer, ForeignKey('expression_alert.id', onupdate="cascade", ondelete="cascade"),
                      nullable=True)

    mathematical_operation_id = Column(Integer, ForeignKey('expression_mathematical_operation.id', onupdate="cascade",
                                                           ondelete="cascade"),
                                       nullable=True)
    function = relationship('ExpressionFunction', cascade="all,delete")
    metric = relationship('ExpressionMetric', cascade="all,delete")
    alert = relationship('ExpressionAlert', cascade="all,delete")
    mathematical_operation = relationship('ExpressionMathematicalOperation', cascade="all,delete")

    def __repr__(self):
        return "<Expression(id='%s', value='%s', unit='%s')>" % \
               (self.id, self.value, self.unit)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "category": self.category,
            "promql": self.promql,
            "constant": self.constant,
            "function": self.function.serialize if self.function is not None else None,
            "metric": self.metric.serialize if self.metric is not None else None,
            "alert": self.alert.serialize if self.alert is not None else None,
            "mathematical_operation": self.mathematical_operation.serialize
            if self.mathematical_operation is not None else None
            # "function_id": self.function_id,
            # "metric_id": self.metric_id,
            # "alert_id": self.alert_id,
            # "mathematical_operation_id": self.mathematical_operation_id
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "expression": self.serialize
        }
        return serialized_obj

    def set(self, json_representation):
        from .ExpressionFunction import ExpressionFunction
        from .ExpressionAlert import ExpressionAlert
        from .ExpressionMetric import ExpressionMetric
        from .ExpressionMathematicalOperation import ExpressionMathematicalOperation

        json_representation = json_representation["expression"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.category = getValIfKeyExists(json_representation, "category")
        self.promql = getValIfKeyExists(json_representation, "promql")
        self.constant = getValIfKeyExists(json_representation, "constant")
        self.function_id = getValIfKeyExists(json_representation, "function_id")
        self.metric_id = getValIfKeyExists(json_representation, "metric_id")
        self.alert_id = getValIfKeyExists(json_representation, "alert_id")
        self.mathematical_operation_id = getValIfKeyExists(json_representation, "mathematical_operation_id")

        if getValIfKeyExists(json_representation, "function") is not None:
            self.function = ExpressionFunction()
            self.function.set(json_representation)
        if getValIfKeyExists(json_representation, "metric") is not None:
            self.metric = ExpressionMetric()
            self.metric.set(json_representation)
        if getValIfKeyExists(json_representation, "alert") is not None:
            self.alert = ExpressionAlert()
            self.alert.set(json_representation)
        if getValIfKeyExists(json_representation, "mathematical_operation") is not None:
            self.mathematical_operation = ExpressionMathematicalOperation()
            self.mathematical_operation.set(json_representation)
