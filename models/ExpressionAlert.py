from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class ExpressionAlert(Base):
    __tablename__ = 'expression_alert'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), nullable=False)
    duration_id = Column(Integer, ForeignKey('duration.id', onupdate="cascade", ondelete="cascade"), nullable=False)
    offset_id = Column(Integer, ForeignKey('offset.id', onupdate="cascade", ondelete="cascade"), nullable=False)

    label_assoc_table = Table('expression_alert__assoc__label', Base.metadata,
                               Column('expression_alert_id', Integer,
                                      ForeignKey('expression_alert.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True),
                               Column('label_id', Integer,
                                      ForeignKey('label.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True)
                               )

    label_list = relationship("Label", secondary='expression_alert__assoc__label', backref="expression_alert",
                              cascade="all,delete")

    duration = relationship("Duration", foreign_keys="ExpressionAlert.duration_id", cascade="all,delete")
    offset = relationship("Offset", foreign_keys="ExpressionAlert.offset_id", cascade="all,delete")

    def __repr__(self):
        return "<ExpressionAlert(id='%s', name='%s', duration_id='%s', offset_id='%s')>" % \
               (self.id, self.name, self.duration_id, self.offset_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "name": self.name,
            "duration": self.duration.serialize if self.duration is not None else None,
            "offset": self.offset.serialize if self.offset is not None else None,
            "label_list": self.serialize_many2many(self.label_list)
            # "duration_id": self.duration_id,
            # "offset_id": self.offset_id
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "alert": self.serialize
        }
        return serialized_obj

    # @property
    def serialize_many2many(self, many2many_property):
        return [item.serialize_w_title for item in many2many_property]


    def set(self, json_representation):
        from .Duration import Duration
        from .Offset import Offset
        from .Label import Label

        json_representation = json_representation["alert"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.name = getValIfKeyExists(json_representation, "name")
        self.duration_id = getValIfKeyExists(json_representation, "duration_id")
        self.offset_id = getValIfKeyExists(json_representation, "offset_id")

        if getValIfKeyExists(json_representation, "duration") is not None:
            self.duration = Duration()
            self.duration.set(json_representation)
        if getValIfKeyExists(json_representation, "offset") is not None:
            self.offset = Offset()
            self.offset.set(json_representation)

        for label in getValIfKeyExists(json_representation, "label_list"):
            label_temp = Label()
            label_temp.set(label)
            self.label_list.append(label_temp)
