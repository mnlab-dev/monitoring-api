from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class ExpressionFunction(Base):
    __tablename__ = 'expression_function'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), nullable=False)

    group_by_label_key_list = relationship('ExpressionFunctionLabelKey', backref="expression_function",
                                           cascade="all,delete")

    def __repr__(self):
        return "<ExpressionFunction(id='%s', name='%s', group_by_label_key_list='%s')>" % \
               (self.id, self.name, self.group_by_label_key_list)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "name": self.name,
            "group_by_label_key_list": self.serialize_many2many(self.group_by_label_key_list)
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "function": self.serialize
        }
        return serialized_obj

    def serialize_many2many(self, many2many_property):
        return [item.serialize_w_title for item in many2many_property]


    def set(self, json_representation):
        from .ExpressionFunctionLabelKey import ExpressionFunctionLabelKey

        json_representation = json_representation["function"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.name = getValIfKeyExists(json_representation, "name")

        for label_key in getValIfKeyExists(json_representation, "group_by_label_key_list"):
            label_key_temp = ExpressionFunctionLabelKey()
            label_key_temp.label_key = label_key
            self.group_by_label_key_list.append(label_key_temp)
