from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class ExpressionFunctionLabelKey(Base):
    __tablename__ = 'expression_function_label_key'

    id = Column(Integer, primary_key=True)
    expression_function_id = Column(Integer, ForeignKey('expression_function.id',
                                                        onupdate="cascade", ondelete="cascade"),
                                    nullable=False)
    label_key = Column(String(60), nullable=False)


    def __repr__(self):
        return "<ExpressionFunctionLabelKey(id='%s',  expression_function_id='%s', label_key='%s')>" % \
               (self.id, self.expression_function_id, self.label_key)

    @property
    def serialize_w_title(self):
        label_key_string = self.label_key
        return label_key_string


    # def set(self, label_key):
    #
    #     json_representation = json_representation["group_by_label_key_list"]
    #
    #     self.id = getValIfKeyExists(json_representation, "id")
    #     self.label_key = getValIfKeyExists(json_representation, "label_key")
