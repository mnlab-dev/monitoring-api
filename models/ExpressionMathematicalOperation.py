from sqlalchemy import Column, ForeignKey, Table
from sqlalchemy import Integer, String, Enum
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class ExpressionMathematicalOperation(Base):
    __tablename__ = 'expression_mathematical_operation'

    id = Column(Integer, primary_key=True)
    mathematical_operator = Column(Enum('+', '-', '*', '/', '%%', '^'), nullable=False)

    expression_assoc_table = Table('expression_mathematical_operation__assoc__expression', Base.metadata,
                                   Column('expression_mathematical_operation_id', Integer,
                                          ForeignKey('expression_mathematical_operation.id',
                                                     onupdate="cascade", ondelete="cascade"),
                                          primary_key=True),
                                   Column('expression_id', Integer,
                                          ForeignKey('expression.id', onupdate="cascade", ondelete="cascade"),
                                          primary_key=True)
                                   )

    expression_list = relationship("Expression", secondary='expression_mathematical_operation__assoc__expression',
                                   backref="target_resource", cascade="all,delete")

    def __repr__(self):
        return "<MathematicalOperation(id='%s', value='%s', unit='%s')>" % \
               (self.id, self.value, self.unit)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "mathematical_operator": self.mathematical_operator,
            "expression_list": self.serialize_many2many(self.expression_list)
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "mathematical_operation": self.serialize
        }
        return serialized_obj

    def serialize_many2many(self, many2many_property):
        return [item.serialize_w_title for item in many2many_property]

    def set(self, json_representation):
        from .Expression import Expression

        json_representation = json_representation["mathematical_operation"]

        self.id = getValIfKeyExists(json_representation, "id")
        self.mathematical_operator = getValIfKeyExists(json_representation, "mathematical_operator")

        for expression in getValIfKeyExists(json_representation, "expression_list"):
            expression_temp = Expression()
            expression_temp.set(expression)
            self.expression_list.append(expression_temp)
