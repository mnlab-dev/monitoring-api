from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String, Enum
from sqlalchemy.orm import relationship
# from helpers.DbHelper import db_session
from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class Label(Base):
    __tablename__ = 'label'

    id = Column(Integer, primary_key=True)
    matching_operator = Column(Enum('=', '=~', '!=', '!~'), nullable=True)
    key = Column(String(60), nullable=False)
    value = Column(String(60), nullable=False)

    def __repr__(self):
        return "<Label(id='%s', matching_operator='%s', key='%s', value='%s')>" % \
               (self.id, self.matching_operator, self.key, self.value)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "matching_operator": self.matching_operator,
            "key": self.key,
            "value": self.value
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "label": self.serialize
        }
        return serialized_obj

    def set(self, label_json):
        label_json = label_json["label"]

        self.id = getValIfKeyExists(label_json, "id")
        self.matching_operator = getValIfKeyExists(label_json, "matching_operator")
        self.key = getValIfKeyExists(label_json, "key")
        self.value = getValIfKeyExists(label_json, "value")
