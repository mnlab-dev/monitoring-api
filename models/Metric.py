from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class Metric(Base):
    __tablename__ = 'metric'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), nullable=False)
    metric_exporter_id = Column(Integer, ForeignKey('metric_exporter.id',
                                                    onupdate="cascade", ondelete="cascade"),
                                nullable=False)

    metric_exporter = relationship("MetricExporter",
                                   foreign_keys='Metric.metric_exporter_id',
                                   back_populates="metric_list", uselist=False, cascade="all,delete")

    def __repr__(self):
        return "<Metric(id='%s', name='%s', metric_exporter_id='%s',)>" % \
               (self.id, self.name, self.metric_exporter_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "name": self.name
            # "metric_exporter_id": self.metric_exporter_id,
            # "metric_exporter": self.metric_exporter.serialize if self.metric_exporter is not None else None
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "metric": self.serialize
        }
        return serialized_obj

    def set(self, metric_json):
        metric_json = metric_json["metric"]

        self.id = getValIfKeyExists(metric_json, "id")
        self.name = getValIfKeyExists(metric_json, "name")
        self.metric_exporter_id = getValIfKeyExists(metric_json, "metric_exporter_id")
