from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class MetricExporter(Base):
    __tablename__ = 'metric_exporter'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), nullable=False)
    # Port is defined as port list for future development where port will be related to target resource too
    metric_exporter_port_list = relationship("MetricExporterPort", back_populates="metric_exporter")
    metric_list = relationship("Metric", back_populates="metric_exporter")

    def __repr__(self):
        return "<MetricExporter(id='%s', name='%s')>" % \
               (self.id, self.name)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "name": self.name,
            "metric_exporter_port_list": self.serialize_list(self.metric_exporter_port_list)
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "metric_exporter": self.serialize
        }
        return serialized_obj

    def serialize_list(self, list_property):
        return [item.serialize_w_title for item in list_property]

    def set(self, dict_to_set):
        dict_to_set = dict_to_set["metric"]

        self.id = getValIfKeyExists(dict_to_set, "id")
        self.name = getValIfKeyExists(dict_to_set, "name")
