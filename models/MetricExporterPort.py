from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from models.Base import Base
from helpers.CommonHelper import getValIfKeyExists

class MetricExporterPort(Base):
    __tablename__ = 'metric_exporter_port'

    id = Column(Integer, primary_key=True)
    port = Column(Integer, nullable=False)
    metric_exporter_id = Column(Integer, ForeignKey('metric_exporter.id',
                                                    onupdate="cascade", ondelete="cascade"),
                                nullable=False)

    metric_exporter = relationship("MetricExporter",
                                   foreign_keys='MetricExporterPort.metric_exporter_id',
                                   back_populates="metric_exporter_port_list", uselist=True, cascade="all,delete")


    def __repr__(self):
        return "<MetricExporterPort(id='%s', port='%s', metric_exporter_id='%s',)>" % \
               (self.id, self.port, self.metric_exporter_id)

    @property
    def serialize(self):
        serialized_obj = {
            "id": self.id,
            "port": self.port,
            "metric_exporter_id": self.metric_exporter_id
        }
        return serialized_obj

    @property
    def serialize_list(self):
        serialized_obj = {
            "id": self.id,
            "port": self.name,
            "metric_exporter_id": self.metric_exporter_id
        }
        return serialized_obj

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "metric_exporter_port": self.serialize
        }
        return serialized_obj

    def set(self, dict_to_set):
        dict_to_set = dict_to_set["metric"]

        self.id = getValIfKeyExists(dict_to_set, "id")
        self.port = getValIfKeyExists(dict_to_set, "port")
        self.metric_exporter_id = getValIfKeyExists(dict_to_set, "metric_exporter_id")
