from sqlalchemy import Column, Table, ForeignKey
from sqlalchemy import Integer, String
from sqlalchemy.orm import relationship

from helpers.CommonHelper import getValIfKeyExists
from models.Base import Base
from models.Metric import Metric
from models.MetricExporter import MetricExporter
from models.MetricExporterPort import MetricExporterPort
from helpers.DbHelper import db_session

class TargetResource(Base):
    __tablename__ = 'target_resource'

    id = Column(Integer, primary_key=True)
    ip = Column(String(60), nullable=False)
    name = Column(String(60), nullable=False)
    ssh_username = Column(String(60), nullable=True)
    ssh_private_key = Column(String(60), nullable=True)
    ssh_password = Column(String(60), nullable=True)

    user_id = Column(Integer, ForeignKey('user.id', onupdate="cascade", ondelete="cascade"), nullable=False)


    label_assoc_table = Table('target_resource__assoc__label', Base.metadata,
                               Column('target_resource_id', Integer,
                                      ForeignKey('target_resource.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True),
                               Column('label_id', Integer,
                                      ForeignKey('label.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True)
                               )

    label_list = relationship("Label", secondary='target_resource__assoc__label', backref="target_resource",
                              cascade="all,delete")

    metric_assoc_table = Table('target_resource__assoc__metric', Base.metadata,
                               Column('target_resource_id', Integer,
                                      ForeignKey('target_resource.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True),
                               Column('metric_id', Integer,
                                      ForeignKey('metric.id', onupdate="cascade", ondelete="cascade"),
                                      primary_key=True)
                               )

    metric_list = relationship("Metric", secondary=metric_assoc_table, backref="target_resource")


    def __repr__(self):
        return "<TargetResource(id='%s', ip='%s', name='%s', ssh_username='%s', ssh_private_key='%s', " \
               "ssh_password='%s', 'user_id'='%s')>" % \
               (self.id, self.ip, self.name, self.ssh_username, self.ssh_private_key, self.ssh_password, self.user_id)


    @property
    def serialize(self):

        target_resource_json = {
            "id": self.id,
            "ip": self.ip,
            "name": self.name,
            "ssh_username": self.ssh_username,
            "ssh_private_key": self.ssh_private_key,
            "ssh_password": self.ssh_password,
            "user_id": self.user_id,
            "label_list": self.serialize_many2many(self.label_list),
            "metric_list": self.serialize_many2many(self.metric_list),
            "port_list": self.get_ports(db=None)
        }
        return target_resource_json

    @property
    def serialize_w_title(self):
        serialized_obj = {
            "target_resource": self.serialize
        }
        return serialized_obj

    def serialize_many2many(self, many2many_property):
        return [item.serialize_w_title for item in many2many_property]

    def set(self, db, target_resource_json):
        from .Label import Label
        from .Metric import Metric
        # from Exporter import Exporter
        # print(json.load(target_resource_json))
        # json_data = _byteify(target_resource_json)
        # json_data = json.load(target_resource_json)

        target_resource_json = target_resource_json["target_resource"]
        # target_resource = Targetresource(ip=target_resource_json["ip"], name=target_resource_json["name"],
        #                              ssh_username=target_resource_json["ssh_username"],
        #                              ssh_password=target_resource_json["ssh_password"],
        #                              ssh_private_key=target_resource_json["ssh_private_key"])
        #
        # for label in target_resource_json["label_list"]:
        #     label_temp = Label(matching_operator=label["label"]["matching_operator"]
        #                        if "matching_operator" in label["label"] else None,
        #                        key=label["label"]["key"]
        #                        if "key" in label["label"] else None,
        #                        value=label["label"]["value"])
        #     target_resource.label_list.append(label_temp)
        # for exporter in target_resource_json["exporter_list"]:
        #     exporter_temp = Exporter(type=exporter["exporter"]["type"],
        #                              allocation_port=exporter["exporter"]["allocation_port"])
        #     for metric_category in exporter["exporter"]["metric_category_list"]:
        #         metric_category_temp = MetricCategory(name=metric_category["metric_category"]["name"])
        #         exporter_temp.metric_category_list.append(metric_category_temp)
        #     target_resource.exporter_list.append(exporter_temp)
        #
        # target_resource_json = json_data["target_resource"]
        # target_resource = Targetresource(ip=target_resource_json["ip"], name=target_resource_json["name"],
        #                              ssh_username=target_resource_json["ssh_username"],
        #                              ssh_password=target_resource_json["ssh_password"],
        #                              ssh_private_key=target_resource_json["ssh_private_key"])

        self.id = getValIfKeyExists(target_resource_json, "id")
        self.ip = getValIfKeyExists(target_resource_json, "ip")
        self.name = getValIfKeyExists(target_resource_json, "name")
        self.ssh_username = getValIfKeyExists(target_resource_json, "ssh_username")
        self.ssh_password = getValIfKeyExists(target_resource_json, "ssh_password")
        self.ssh_private_key = getValIfKeyExists(target_resource_json, "ssh_private_key")

        self.user_id = getValIfKeyExists(target_resource_json, "user_id")


        self.metric_list = [db.query(Metric).filter_by(name=item["metric"]["name"]).one()
                            for item in getValIfKeyExists(target_resource_json, "metric_list")]
        # self.metric_list = [db.query(Metric).get(item["metric"]["id"]) if "id" in item["metric"]
        #                     else db.query(Metric).filter(name=item["metric"]["name"])
        #                     for item in getValIfKeyExists(target_resource_json, "metric_list")]
        self.label_list = []
        for label in getValIfKeyExists(target_resource_json, "label_list"):
            label_temp = Label()
            label_temp.set(label)
            self.label_list.append(label_temp)

    def create(self, db):
        # with db_session() as db:
        db.add(self)
        db.flush()
        db.refresh(self)
        return_id = self.id
        return return_id

    def get_by_filter(self, db):
        target_resources = db.query(TargetResource) \
                       .filter(TargetResource.id == self.id if self.id != None else True)\
                       .filter(TargetResource.ip == self.ip if self.ip != None else True)\
                       .filter(TargetResource.name == self.name if self.name != None else True)\
                       .filter(TargetResource.ssh_username == self.ssh_username
                                                              if self.ssh_username != None else True)\
                       .filter(TargetResource.ssh_private_key == self.ssh_private_key
                                                              if self.ssh_private_key != None else True)\
                       .filter(TargetResource.ssh_password == self.ssh_password
                                                              if self.ssh_password != None else True)\
                       .filter(TargetResource.user_id == self.user_id if self.user_id != None else True)\
                       .all()

        target_resources_json = []
        for target_resource in target_resources:
            target_resource_json = target_resource.serialize_w_title
            target_resources_json.append(target_resource_json)

        return target_resources_json

    def update(self, db, json_content):
        # TODO: Join and delete probably better
        for label in self.label_list:
            db.delete(label)
        self.set(db, json_content)


    def delete(self, db):
        temp_obj = db.query(TargetResource).get(self.id)
        db.delete(temp_obj)
        db.flush()


    def get_ports(self, db):
        def get_distinct(seq):
            seen = set()
            seen_add = seen.add
            return [x for x in seq if not (x in seen or seen_add(x))]

        if db is None:
            with db_session() as db:
                port_lists = db.query(MetricExporterPort.port)\
                    .join(Metric, TargetResource.metric_list).join(MetricExporter, Metric.metric_exporter)\
                    .join(MetricExporterPort, MetricExporter.metric_exporter_port_list)\
                    .filter(TargetResource.id == self.id)\
                    .order_by(MetricExporterPort.port)\
                    .all()
        else:
            port_lists = db.query(MetricExporterPort.port)\
                .join(Metric, TargetResource.metric_list).join(MetricExporter, Metric.metric_exporter)\
                .join(MetricExporterPort, MetricExporter.metric_exporter_port_list)\
                .filter(TargetResource.id == self.id)\
                .order_by(MetricExporterPort.port)\
                .all()

        return get_distinct([port_list[0] for port_list in port_lists])
