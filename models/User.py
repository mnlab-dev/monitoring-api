from sqlalchemy import Column, Table, ForeignKey
from sqlalchemy import Integer, String, Enum, event
from sqlalchemy.orm import relationship

# from sqlalchemy.orm import sessionmaker
from helpers.DbHelper import Session
from sqlalchemy import create_engine

from helpers.DbHelper import on_connect, delete_label_orphans, db_session

from configuration import config as cnf
from models.Base import Base
from models.Label import Label

from helpers.CommonHelper import getValIfKeyExists


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    name = Column(String(60), nullable=False)
    role = Column(Enum('admin', 'user'), nullable=False)

    def __repr__(self):
        return "<User(id='%s', name='%s', role='%s')>" % \
               (self.id, self.name, self.role)

    target_resource = relationship("TargetResource", backref="user", cascade="all,delete")
    alert_rule = relationship("AlertRule", backref="user", cascade="all,delete")

    @property
    def serialize(self):
        user_json = {
            "user": {
                "id": self.id,
                "name": self.name,
                "role": self.role
            }
        }

        return user_json


    def get_by_filter(self):
        with db_session() as db:
            users = db.query(User) \
                .filter(User.id == self.id if self.id != None else True) \
                .filter(User.name == self.name if self.name != None else True) \
                .filter(User.role == self.role if self.role != None else True) \
                .all()

            ## Needed if @property is removed - TO investigate why
            users_json = []
            for user in users:
                print(user)
                user_json = user.serialize
                users_json.append(user_json)

            db.commit()

        return users_json

    def set(self, user_json):
        user_json = getValIfKeyExists(user_json, "user")

        self.id = getValIfKeyExists(user_json, "id")
        self.name = getValIfKeyExists(user_json, "name")
        self.role = getValIfKeyExists(user_json, "role")

    def create(self):
        with db_session() as db:
            db.add(self)
            db.flush()
            db.refresh(self)
            return_id = self.id
            db.commit()
        return return_id

    def update(self):
        with db_session() as db:
            db.merge(self)
            db.flush()
            db.commit()
        return "update_done"

    def delete(self):
        with db_session() as db:
            # db.query(User).filter_by(id=self.id).delete()
            temp_obj = db.query(User).get(self.id)
            db.delete(temp_obj)
            db.flush()
            db.commit()
        return "delete_done"


