import jinja2

var_id = 1
# In this case, we will load templates off the filesystem.
# This means we must construct a FileSystemLoader object.
#
# The search path can be used to make finding templates by
#   relative paths much easier.  In this case, we are using
#   absolute paths and thus set it to the filesystem root.
templateLoader = jinja2.FileSystemLoader(searchpath="../helpers/templates/")

# An environment provides the data necessary to read and
#   parse our templates.  We pass in the loader object here.
templateEnv = jinja2.Environment(loader=templateLoader, extensions=['jinja2.ext.loopcontrols'])

# This constant string specifies the template file we will use.
TEMPLATE_FILE = "target_resource.j2"

# Read the template file using the environment object.
# This also constructs our Template object.
template = templateEnv.get_template(TEMPLATE_FILE)

# Specify any input variables to the template as a dictionary.
templateVars = {
    "target_resource": {
        "ip": "10.30.0.200",
        "port_list": [
            9091,
            10000
        ],
        "label_list": [
            {"label": {
                "key": "job",
                "value": "node"
            }},
            {"label": {
                "key": "any_key",
                "value": "any_val"
            }}
        ]
    }
}

# Finally, process the template to produce our final text.
outputText = template.render(templateVars)

print(outputText)

jsonFile = open("/home/yanos/" + str(var_id) + ".json", "w+")
jsonFile.write(outputText)
jsonFile.close()
