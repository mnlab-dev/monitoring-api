import jinja2

var_id = 777
# In this case, we will load templates off the filesystem.
# This means we must construct a FileSystemLoader object.
#
# The search path can be used to make finding templates by
#   relative paths much easier.  In this case, we are using
#   absolute paths and thus set it to the filesystem root.
templateLoader = jinja2.FileSystemLoader(searchpath="../helpers/templates/")

# An environment provides the data necessary to read and
#   parse our templates.  We pass in the loader object here.
templateEnv = jinja2.Environment(loader=templateLoader, extensions=['jinja2.ext.loopcontrols'])

# This constant string specifies the template file we will use.
TEMPLATE_FILE = "alert_rule.j2"

# Read the template file using the environment object.
# This also constructs our Template object.
template = templateEnv.get_template(TEMPLATE_FILE)

# metric1{instance="SERVER1"} > 10 AND ignoring(instance) metric2{instance="SERVER2"} > 10

# Specify any input variables to the template as a dictionary.
templateVars = {
  "alert_rule": {
    "user_id": 1,
    "name": "cpu_GT_80_Alert",
    "duration": {
      "value": 10,
      "unit": "m"
    },
    "annotation_summary":  "Alert if cpu > 80% and DOS_Attack_Alert is firing",
    "annotation_description": "This alert will fire when the average value of the metric named 'cpu_used_percentage' for the last 5 minutes of the instance whose IP is '192.168.1.10' exceeds the value '80' for the duration of 10 minutes and the IDS alert named 'DOS_Attack_Alert' is also found to be true (=1) during the same time"
    ,
    "condition": {
      "category": "logical_operation",
      "logical_operation": {
        "logical_operator": "AND",
        "condition_list": [
          {
            "condition": {
              "category": "inequality",
              "inequality": {
                "LHS": {
                  "expression": {
                    "category": "metric",
                    "function": {
                        "name": "avg",
                        "group_by_label_key_list": [
                            "instance"
                          ]
                      },
                    "metric": {
                      "name": "cpu_used_percentage",
                      "label_list": [
                        {
                          "label": {
                            "key": "instance",
                            "value": "192.168.1.10",
                            "matching_operator": "="
                          }
                        }
                      ],
                      "duration": {
                        "value": 5,
                        "unit": "m"
                      },
                      "offset": {
                        "value": 0,
                        "unit": "m"
                      }
                    }
                  }
                },
                "comparison_operator": ">",
                "RHS": {
                  "expression": {
                    "category": "constant",
                    "constant": 80
                  }
                }
              }
            }
          },
          {
            "condition": {
              "category": "inequality",
              "inequality": {
                "LHS": {
                  "expression": {
                    "category": "metric",
                    "metric": {
                      "name": "up",
                      "label_list": [
                        {
                          "label": {
                            "key": "instance",
                            "value": "192.168.1.10",
                            "matching_operator": "="
                          }
                        }
                      ],
                      "duration": {
                        "value": 5,
                        "unit": "m"
                      },
                      "offset": {
                        "value": 0,
                        "unit": "s"
                      }
                    }
                  }
                },
                "comparison_operator": "==",
                "RHS": {
                  "expression": {
                    "category": "constant",
                    "constant": 1
                  }
                }
              }
            }
          }
        ]
      }
    }
  }
}



# Finally, process the template to produce our final text.
outputText = template.render(templateVars)

print(outputText)

jsonFile = open("/home/yanos/" + str(var_id) + ".yml", "w+")
jsonFile.write(outputText)
jsonFile.close()
