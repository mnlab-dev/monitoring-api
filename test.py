from models.Label import Label
from contextlib import contextmanager
from sqlalchemy import create_engine, event
from sqlalchemy.orm import scoped_session, sessionmaker

from configuration import config as cnf
from models import TargetResource

def on_connect(conn, record):
    conn.execute('pragma foreign_keys=ON')


#connection_string = "sqlite:///" + cnf.DATABASE_PATH
#connection_string = "mysql://steve:#cids@localhost/DataBasemnl"
#connection_string = "mysql:/user:passwd@127.0.0.1:3306/"
db_type = "mysql"

engine = create_engine( db_type + cnf.DATABASE_CONN_STRING, echo=True)
event.listen(engine, 'connect', on_connect)
connection = engine.connect()
db_sess = scoped_session(sessionmaker(autocommit=False, autoflush=True, bind=engine))

print(db_sess.query(TargetResource.TargetResource).all())


db_sess.remove()
connection.close()



